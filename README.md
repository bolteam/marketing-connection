# MarketingConnection

Gem to integrate marketing with a ruby project

## Installation

Add this line to your application's Gemfile:

    gem 'marketing_connection'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install marketing_connection

## Usage

    #Intialize connection
    marketing_connection = MarketingConnection::Base.new(api_key: "xx", api_secret: "yy")


## Lists

    #create a list
    marketing_connection.lists.create(params)

    #List index
    marketing_connection.lists.list

    #Find a list
    marketing_connection.lists.find(list_id)

## Templates

    #create a list
    marketing_connection.templates.create(params)

    #List index
    marketing_connection.templates.list

    #Find a list
    marketing_connection.templates.find(template_id)

## Deliveries

    #create a list
    marketing_connection.deliveries.create(params)

    #List index
    marketing_connection.deliveries.list

    #Find a list
    marketing_connection.deliveries.find(list_id)



## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
