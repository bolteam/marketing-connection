require "marketing_connection/version"
require "marketing_connection/delivery"
require "marketing_connection/error"
require "marketing_connection/gun"
require "marketing_connection/list"
require "marketing_connection/template"

module MarketingConnection

  class Base

    def initialize(opts = {})
      puts opts.inspect
    end

    def lists
      MarketingConnection::List.new(self)
    end

    def deliveries
      MarketingConnection::Delivery.new
    end

    def templates
      MarketingConnection::Template.new(self)
    end

    def guns
      MarketingConnection::Gun.new(self)
    end

  end




  class << self
    attr_accessor :api_key, :api_secret, :mode, :api_url

    def mode
      @mode || "demo"
    end

    def api_url
      if self.mode == "production"
        return "email-marketing.herokuapp.com/"
      else
        return @api_url
      end
    end

    # Submits the API call to the Mailgun server
    def submit(method, target_url, parameters={})
      url = base_url + target_url
      puts url
      begin
        parameters = {:params => parameters} if method == :get
        return JSON(RestClient.send(method, url, parameters))
      rescue => e
        error_message = nil
        if e.respond_to? :http_body
          begin
            error_message = JSON(e.http_body)["message"]
          rescue
            raise e
          end
          raise MarketingConnection::Error.new(error_message)
        end
        raise e
      end
    end

    def base_url
      "http://#{self.api_key}:#{self.api_secret}@#{self.api_url}/api/v1/"
    end

    def config
      yield self
      true
    end
  end

end
