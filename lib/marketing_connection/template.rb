module MarketingConnection
  class Template
    def initialize(opts = {})
    end

    def create(params)
      MarketingConnection.submit(:post, template_url, params)
    end

    def list(opts = {})
      MarketingConnection.submit(:get, template_url, opts)
    end

    def find(template_id)
      MarketingConnection.submit(:get, "#{template_url}/#{template_id}")
    end

    def update
    end

    private
      def template_url
        "templates"
      end


  end
end
