module MarketingConnection
  class List
    def initialize(opts = {})

    end

    def list(opts = {})
      MarketingConnection.submit(:get, list_url, opts)
    end

    def create(params = {})
      MarketingConnection.submit(:post, list_url, params)
    end

    def create_csv(params = {})
      MarketingConnection.submit(:post, "#{list_url}/create_csv", params)
    end

    def update
    end

    def find(list_id)
      MarketingConnection.submit(:get, "#{list_url}/#{list_id}")
    end

    private
      def list_url
        "lists"
      end
  end
end
